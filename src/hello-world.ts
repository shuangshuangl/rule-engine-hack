import { Engine } from "json-rules-engine";
import * as rule from "../rules/test.json";


async function start() {
    const engine = new Engine();
    const { all } = rule;
    
    engine.addRule({
        conditions: { all },

        // conditions: { // define the contiditons for when hello world should display
        //     all: [{
        //       fact: 'displayMessage',
        //       operator: 'equal',
        //       value: true
        //     }]
        // },
        event: {
            type: 'message',
            params: {
                data: 'hello-world-rule-engine'
            }
        }
    })


    
    const facts = { displayMessage: true };

    const { events } = await engine.run(facts);

    events.map(event => console.log(event.params.data));
}

start();
