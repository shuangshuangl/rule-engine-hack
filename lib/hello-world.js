"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const json_rules_engine_1 = require("json-rules-engine");
//import * as rule from "../rules/test.json";
async function start() {
    const engine = new json_rules_engine_1.Engine();
    //const { all } = rule;
    engine.addRule({
        // conditions: { all
        // },
        conditions: {
            all: [{
                    fact: 'displayMessage',
                    operator: 'equal',
                    value: true
                }]
        },
        event: {
            type: 'message',
            params: {
                data: 'hello-world-rule-engine'
            }
        }
    });
    const facts = { displayMessage: true };
    const { events } = await engine.run(facts);
    events.map(event => console.log(event.params.data));
}
start();
//# sourceMappingURL=hello-world.js.map