"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const json_rules_engine_1 = require("json-rules-engine");
const rule = __importStar(require("../rules/test.json"));
async function start() {
    const engine = new json_rules_engine_1.Engine();
    const { all } = rule;
    engine.addRule({
        conditions: { all },
        // conditions: {
        //     all: [{
        //       fact: 'displayMessage',
        //       operator: 'equal',
        //       value: true
        //     }]
        // },
        event: {
            type: 'message',
            params: {
                data: 'hello-world-rule-engine'
            }
        }
    });
    const facts = { displayMessage: true };
    const { events } = await engine.run(facts);
    events.map(event => console.log(event.params.data));
}
start();
//# sourceMappingURL=hello-world.js.map